# JSON Pretty Printer

JSON Pretty Printer for LabVIEW.

The native LabVIEW function does not provide the functions to pretty print your JSON.

This library includes two simple VIs that can be used to:

- Pretty Print (human-readable with space and tabs);
- Minify (remove all white-spaces and comments);

## Installation and Usage
This library can be installed using VIPM or downloading the source code directly.

As it does not have any direct dependency, it should work on both.

![docs/snippet.png](/docs/Snippet.png)

## Compatibility and Dependencies
- LabVIEW 2020 or later (VIPM Package)
- There are no dependencies. 

## Author
- Felipe Pinheiro Silva

### Contributors
- Jim Kring

## Contributing

Merge requests are welcome. Open an issue before.

### LabVIEW Version
This code was developed using LabVIEW 2020 Community Edition.

### Testing
For testing, this project uses Caraya Unit Test Framework (available at VIPM).
